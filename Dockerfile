FROM jdk8


RUN mkdir -p /home/java/bittable-apiserver/
RUN mkdir /home/java/bittable-apiserver/resources

COPY ./target/bittable-api*.jar /home/java/bittable-apiserver/bittable-apiserver.jar

COPY ./src/main/resources/*.properties /home/java/bittable-server/resources/

CMD ["java", "-Dloader.path=/home/java/bittable-apiserver/resources", "-Dserver.port=8080", "-jar", "/home/java/bittable-apiserver/bittable-apiserver.jar"]


