package com.bittable.apiserver.config

import com.google.common.collect.Sets.newHashSet
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.context.request.async.DeferredResult
import springfox.documentation.builders.PathSelectors
import springfox.documentation.builders.RequestHandlerSelectors
import springfox.documentation.builders.ResponseMessageBuilder
import springfox.documentation.schema.ModelRef
import springfox.documentation.service.ApiInfo
import springfox.documentation.service.Parameter
import springfox.documentation.service.ResponseMessage
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spring.web.plugins.Docket
import springfox.documentation.swagger.web.UiConfiguration
import springfox.documentation.swagger2.annotations.EnableSwagger2
import java.util.*

@Configuration
@EnableSwagger2
class SwaggerConfig {

    @Bean
    internal fun UiApi(): Docket {
        return Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build()
//                .forCodeGeneration(false)
//                .groupName("api")
//                .useDefaultResponseMessages(false)
//                .produces(newHashSet<String>("application/json"))
//                .genericModelSubstitutes(DeferredResult::class.java)
//                .select().apis(RequestHandlerSelectors.basePackage("com.bittable.apiserver.controller")).build()
//                .globalOperationParameters(globalOperationParameters(true))
//                .globalResponseMessage(RequestMethod.GET, globalResponseMessage())
//                .globalResponseMessage(RequestMethod.POST, globalResponseMessage())
//                .apiInfo(uiApiInfo())
    }

//    private fun uiApiInfo(): ApiInfo {
//        return ApiInfo(
//                "API文档 - (前端UI篇)",
//                "新版API文档</br>" + "1、开发平台：<a href='http://api.bittable.com/swagger-ui.html' target='_top'>http://api.bittable.com/swagger-ui.html</a>",
//                "v1.0", null, "Bittable", null, null)
//    }
//
//    /**
//     * 全局请求头参数
//     */
//    private fun globalOperationParameters(needUserLogin: Boolean): List<Parameter> {
//        return ArrayList()
//    }
//
//    /**
//     * 请求返回的HTTP全局状态码说明(非业务状态码)
//     */
//    private fun globalResponseMessage(): List<ResponseMessage> {
//        val messageList = ArrayList<ResponseMessage>()
//        messageList.add(ResponseMessageBuilder().code(400).message("请求无效").build())
//        messageList.add(ResponseMessageBuilder().code(403).message("资源不可用").build())
//        messageList.add(ResponseMessageBuilder().code(404).message("服务器未能找到相关信息").build())
//        messageList.add(ResponseMessageBuilder().code(500).message("系统内部错误").responseModel(ModelRef("Error")).build())
//        return messageList
//    }
//
//
//    @Bean
//    internal fun uiConfig(): UiConfiguration {
//        //validatorUrl, docExpansion, apiSorter, defaultModelRendering, String[] supportedSubmitMethods, enableJsonEditor, showRequestHeaders
//        return UiConfiguration(null, "none", "alpha", "model", UiConfiguration.Constants.DEFAULT_SUBMIT_METHODS, false, false)
//    }

}
