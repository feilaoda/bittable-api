package com.bittable.apiserver.config

import io.ebean.EbeanServer
import io.ebean.EbeanServerFactory
import io.ebean.config.ServerConfig
import io.ebean.spring.txn.SpringJdbcTransactionManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cache.interceptor.KeyGenerator
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import javax.sql.DataSource


@Configuration
class CacheConfig {

    @Bean
    fun customKeyGenerator(): KeyGenerator {
        return KeyGenerator { o, method, objects ->
            val sb = StringBuilder()
            sb.append("bit-")
            sb.append(method.name).append("-")
            for (obj in objects) {
                sb.append(obj.toString()).append(":")
            }
            sb.toString()
        }
    }

}
