package com.bittable.apiserver.config

import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import javax.sql.DataSource

@Configuration
@ConfigurationProperties(prefix = "spring.datasource")
class DataSourceConfig : HikariConfig() {
    @Bean
    @Primary
    fun initDataSource(): DataSource {
        return HikariDataSource(this)
    }
}
