package com.bittable.apiserver.config

import com.bittable.apiserver.rest.Result
import org.slf4j.LoggerFactory
import org.springframework.web.context.request.RequestContextHolder
import org.springframework.web.context.request.ServletRequestAttributes
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import kotlin.reflect.KClass

/**
 * Created by feilaoda on 2017/7/13.
 */
fun <T:Any> loggerFor(clazz: KClass<T>) = LoggerFactory.getLogger(clazz.java)

fun httpServletRequest(): HttpServletRequest {
    return (RequestContextHolder.getRequestAttributes() as ServletRequestAttributes).request
}

fun httpServletResponse(): HttpServletResponse? {
    return (RequestContextHolder.getRequestAttributes() as ServletRequestAttributes).response
}

fun httpRedirect(path: String){
    val response = httpServletResponse()
    response?.sendRedirect(path)
}


