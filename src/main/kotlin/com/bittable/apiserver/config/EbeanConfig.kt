package com.bittable.apiserver.config

import io.ebean.EbeanServer
import io.ebean.EbeanServerFactory
import io.ebean.config.ServerConfig
import io.ebean.spring.txn.SpringJdbcTransactionManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import javax.sql.DataSource


@Configuration
class EbeanConfig {

    @Autowired
    internal var dataSource: DataSource? = null

    @Bean
    fun ebeanServer(): EbeanServer {
        val config = ServerConfig()
        config.name = "datasource"
        config.packages = listOf("com.bittable")
        config.dataSource = dataSource
        config.externalTransactionManager = SpringJdbcTransactionManager()

        //    // set the spring's datasource and transaction manager.
        //config.setExternalTransactionManager(new SpringAwareJdbcTransactionManager());

        config.isDefaultServer = true
        config.isRegister = true

        config.loadFromProperties()

        // set as default and register so that Model can be
        // used if desired for save() and update() etc

        return EbeanServerFactory.create(config)
    }


}
