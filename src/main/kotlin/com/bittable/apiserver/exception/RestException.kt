package com.bittable.apiserver.exception

/**
 * Created by feilaoda on 2017/7/14.
 */
class RestException(code: Int, message: String) : RuntimeException(message) {
    var code: Int = 0
        internal set

    init {
        this.code = code
    }
}
