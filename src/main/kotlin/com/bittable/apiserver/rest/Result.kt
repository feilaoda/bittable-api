package com.bittable.apiserver.rest

import com.alibaba.fastjson.JSON
import com.bittable.apiserver.exception.RestException

/**
 * Created by feilaoda on 2017/7/13.
 */
class Result<T>() {
    var code: Int? = 200
    var msg: String? = null
    var data: T? = null
    var datas: List<T>? = null


    fun ok(): Result<T> {
        this.code = 200
        return this
    }

    fun ok(data: T): Result<T> {
        this.code = 200
        this.data = data
        return this
    }

    fun ok(datas: List<T>): Result<T> {
        this.code = 200
        this.datas = datas
        return this
    }


    fun error(code: Int, message: String): Result<T> {
        this.code = code
        this.msg = message
        return this
    }

    fun error400(message: String): Result<T> {
        val res = error(400, message)
        throw RestException(400, JSON.toJSONString(res))
    }

    fun error404(message: String): Result<T> {
        val res = error(404, message)
        throw RestException(404, JSON.toJSONString(res))
    }

    fun error500(message: String): Result<T> {
        val res = error(500, message)
        throw RestException(500, JSON.toJSONString(res))
    }

//
//
//    companion object {
//
//        val UNKNOWN_ERROR = 1000
//        val TOKEN_ERROR = 1001
//
//         fun ok(): Result<T> {
//            return Result<T>(200, null)
//        }
//
//        fun ok(obj: Any?): Result<T> {
//            val res =  Result(200, null)
//        }
//
//        fun error(obj: String): Result {
//            return Result(500, obj)
//        }
//
//        fun error(code: Int, obj: Any?): Result {
//            return Result(code, obj)
//        }
//
//        fun error(code: Int, message: String): Result {
//            return Result().add("code", code).add("msg", message)
//        }
//
//        fun error400(message: String): Result {
//            val res = error(400, message)
//            throw RestException(400, JSON.toJSONString(res))
//        }
//
//        fun error404(message: String): Result {
//            val res = error(404, message)
//            throw RestException(404, JSON.toJSONString(res))
//        }
//
//        fun error500(message: String): Result {
//            val res = error(500, message)
//            throw RestException(500, JSON.toJSONString(res))
//        }
//    }
}

