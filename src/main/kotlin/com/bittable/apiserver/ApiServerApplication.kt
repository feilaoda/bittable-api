package com.bittable.apiserver

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.builder.SpringApplicationBuilder
import org.springframework.boot.runApplication
import org.springframework.cache.annotation.EnableCaching
import org.springframework.cache.interceptor.KeyGenerator
import org.springframework.context.annotation.Bean
import java.lang.reflect.Method


@SpringBootApplication
@EnableCaching
class ApiServerApplication

fun main(args: Array<String>) {
    System.setProperty("ebean.props.file", "application.properties")
    runApplication<ApiServerApplication>(*args)
}
