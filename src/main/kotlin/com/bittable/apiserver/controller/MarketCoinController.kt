package com.bittable.apiserver.controller

import com.bittable.apiserver.cache.CacheService
import com.bittable.apiserver.exception.RestException
import com.bittable.apiserver.form.MarketCoinForm
import com.bittable.apiserver.form.MarketTradeForm
import com.bittable.apiserver.model.*
import com.bittable.apiserver.rest.Result
import com.bittable.apiserver.vo.*
import io.ebean.Finder
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import java.util.*
import javax.validation.Valid

@RestController
@RequestMapping("/v1/market")
@Api
class MarketCoinController {

    @Autowired
    val cacheService: CacheService? = null


    @PostMapping("/global")
    fun  getGlobal(): Result<MarketSummaryData> {
        val summaryData = MarketSummaryData()
        summaryData.usdcny = 6.83f

        var coins = MarketCoin.finder.query().where().setMaxRows(5).findList()

        var datas = listOf<MarketCoinSummary>()
        for(coin in coins) {
            var data = MarketCoinSummary()
            data.name = coin.name
            data.title = coin.title
            data.symbol = coin.symbol
            data.price = coin.price
            datas += data
        }
        summaryData.tops = datas
        summaryData.lastUpdateDate = System.currentTimeMillis()
        return Result<MarketSummaryData>().ok(summaryData)
    }

    @PostMapping("/coins")
    @ApiOperation("获取数字币列表")
    fun getCoins(@Valid @ModelAttribute coinForm: MarketCoinForm): Result<MarketCoinData> {
        var size = coinForm.size

        var first = (coinForm.p - 1) * size
        var query = MarketCoin.finder.query().where().eq("status", 1)
                .setOrderBy("rank")
                .setFirstRow(first).setMaxRows(size)

        var coins = query.findList()
        var datas = listOf<MarketCoinData>()
        for (coin in coins) {
            val data = convertCoin(coin)
            datas += data
        }
        return Result<MarketCoinData>().ok(datas)
    }



    @RequestMapping("/trade/{coin}")
    @ApiOperation("获取行情接口")
    fun getCoinCap(@PathVariable("coin") coinName: String,@Valid @ModelAttribute capForm: MarketTradeForm): Result<MarketTradeData> {

//        val coinOption = MarketCoin.finder.query().where().eq("name", coinName).findOneOrEmpty()
//        if(!coinOption.isPresent) {
//            throw RestException(404, "can't find %coinName")
//        }
//        val coin = coinOption.get()

        val coin = cacheService?.getCoin(coinName)
        if(coin == null) {
            throw RestException(404, "can't find %coinName")
        }

        val tradeData = MarketTradeData()

//        type:("数据类型：5：1天数据   15：7天数据  60：1个月数据  120：3个月数据  1440：1年数据  2880:全部数据")

        var dataType = 2880

        if(capForm.start == null || capForm.end == null) {
            dataType = 2880
            capForm.end = System.currentTimeMillis()
        }else {
            val delta = capForm.end - capForm.start
            val TIME_ALL: Long = 3*365*24*3600*1000L

            if(delta >= 3*365*24*3600*1000L) {
                dataType = 2880
            }else if(delta >= 365*24*3600*1000L) {
                dataType = 1440
            }else if(delta >= 90*24*3600*1000L) {
                dataType = 120
            }else if(delta >= 30*24*3600*1000L) {
                dataType = 60
            }else if(delta > 24*3600*1000L) {
                dataType = 15
            }else {
                dataType = 5
            }
        }

        var tradeSize = 0



        val siteId = 1


        var coinId:Long = coin.id ?: 0
        val trades = cacheService?.getTrades(coinId, capForm.start, capForm.end, dataType)
        tradeSize = trades?.size ?: 0

        val priceUsdArray: Array<Any?> = arrayOfNulls(tradeSize)
        val priceBtcArray: Array<Any?> = arrayOfNulls(tradeSize)
        val marketCapArray: Array<Any?> = arrayOfNulls(tradeSize)
        val volumeArray: Array<Any?> = arrayOfNulls(tradeSize)

        var i = 0
        for(trade in trades?: listOf()) {
            var time = trade.tid
            val timeKey = time.toString()
            priceUsdArray[i] = arrayOf(time, trade.priceUsd) //, trade.priceUsd, trade.volume)
            marketCapArray[i] = arrayOf(time, trade.marketCapUsd)
            volumeArray[i] = arrayOf(time, trade.volume)
            priceBtcArray[i] = arrayOf(time, trade.priceBtc)
            i++
        }


        tradeData.marketCaps = marketCapArray
        tradeData.priceUsds = priceUsdArray
        tradeData.priceBtcs = priceBtcArray
        tradeData.volumes = volumeArray

        return Result<MarketTradeData>().ok(tradeData)
    }

    @PostMapping("/currency/{coin}")
    @ApiOperation("获取行情接口")
    fun getCoinInfo(@PathVariable("coin") coinName: String): Result<MarketCoinDetailData> {

        val coinOption = MarketCoin.finder.query().where().eq("name", coinName).findOneOrEmpty()
        if(!coinOption.isPresent) {
            throw RestException(404, "can't find %coinName")
        }
        val coin = coinOption.get()

        val tradeData = MarketCoinDetailData()
        tradeData.name = coin.name
        tradeData.title = coin.title
        tradeData.symbol = coin.symbol
        tradeData.price = coin.price
        tradeData.marketCap = coin.marketCap
        tradeData.volume = coin.volume
        tradeData.supply = coin.supply
        tradeData.change24h = coin.change24h?.toFloat()
        tradeData.browser1 = coin.browser1
        tradeData.browser2 = coin.browser2
        tradeData.browser3 = coin.browser3
        tradeData.src = coin.src
        tradeData.website = coin.website
        tradeData.logo = coin.logo
        tradeData.rank = coin.rank


        return Result<MarketCoinDetailData>().ok(tradeData)
    }

    fun convertCoin(coin: MarketCoin): MarketCoinData {
        var data = MarketCoinData()
        data.name = coin.name
        data.title = coin.title
        data.rank = coin.rank
        data.symbol = coin.symbol
        data.logo = coin.logo
        data.marketCap = coin.marketCap
        data.price = coin.price
        data.supply = coin.supply
        data.volume = coin.volume
        data.change1h = coin.change1h?.toFloat()
        data.change6h = coin.change6h?.toFloat()
        data.change24h = coin.change24h?.toFloat()
        data.change7d = coin.change7d?.toFloat()
        return data
    }



    fun getTradeFinder(dataType: Int): Finder<Long, out MarketTrade> {

        return MarketTrade.finder

//        val list = ArrayList<String>()
//        when(dataType) {
//            5-> {
//                return MarketTrade5Min.finder
//            }
//            15-> {
//                return MarketTrade7Day.finder
//            }
//            60-> {
//                return MarketTrade1Month.finder
//            }
//            120-> {
//                return MarketTrade3Month.finder
//            }
//            1440-> {
//                return MarketTrade1Year.finder
//            }
//            2880-> {
//                return MarketTradeAll.finder
//            }
//            else -> {
//                return MarketTradeAll.finder
//            }
//        }

    }

}


