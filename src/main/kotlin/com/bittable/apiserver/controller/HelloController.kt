package com.bittable.apiserver.controller

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.util.*

@RestController
@RequestMapping("/v1/hello")
class HelloController {


    @GetMapping("")
    fun hello(): String {
        return "HelloWorld " + Date()
    }
}
