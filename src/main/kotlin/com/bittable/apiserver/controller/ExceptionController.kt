package com.bittable.apiserver.controller

import com.bittable.apiserver.config.loggerFor
import com.bittable.apiserver.exception.RestException
import org.slf4j.Logger
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@ControllerAdvice
class ExceptionHandlingController {
    val logger: Logger = loggerFor(ExceptionHandlingController::class)

    @ExceptionHandler(Exception::class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    fun handleError(req: HttpServletRequest, response: HttpServletResponse, ex: Exception) {

        try {
            logger.error(ex.message, ex)
            response.writer.write(ex.message)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    @ExceptionHandler(RestException::class)
    fun handleRestError(req: HttpServletRequest, response: HttpServletResponse, ex: RestException) {

        try {
            response.status = ex.code
            response.writer.write(ex.message)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }
}
