package com.bittable.apiserver.cache

import com.bittable.apiserver.exception.RestException
import com.bittable.apiserver.model.MarketCoin
import com.bittable.apiserver.model.MarketTrade
import io.ebean.Finder
import org.springframework.cache.annotation.Cacheable
import org.springframework.stereotype.Service

@Service
class CacheService {


//    @Cacheable(value = "default", key = "{#root.methodName, #coinId, #end, #dataType}")
    fun getTrades(coinId:Long, start:Long, end:Long, dataType: Int): List<MarketTrade> {
        val siteId = 1

        var finder = getTradeFinder(dataType)
        val query = finder.query().where().eq("coinId", coinId)
                .eq("siteId",siteId)
                .eq("dataType", dataType)

        if(start != null) {
            query.ge("tid", start)
        }

        query.le("tid", end)
                .setOrderBy("tid")
        val trades = query.setMaxRows(2000).findList()

        return trades
    }


    fun getTradeFinder(dataType: Int): Finder<Long, out MarketTrade> {
        return MarketTrade.finder
    }


    @Cacheable(value = "default", keyGenerator = "customKeyGenerator")
    fun getCoin(name: String): MarketCoin? {
        val coinOption = MarketCoin.finder.query().where().eq("name", name).findOneOrEmpty()
        if(!coinOption.isPresent) {
            return null
        }
        return coinOption.get()
    }

}