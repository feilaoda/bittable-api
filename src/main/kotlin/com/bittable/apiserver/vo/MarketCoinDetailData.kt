package com.bittable.apiserver.vo

import com.alibaba.fastjson.JSONObject
import com.fasterxml.jackson.annotation.JsonProperty
import io.swagger.annotations.ApiModel
import java.math.BigDecimal
import com.fasterxml.jackson.annotation.JsonCreator



@ApiModel("价格数组，第一元素：时间戳，第二：市值，第三：价格，第四：交易量")
class MarketCoinDetailData(

) {
    var name: String? = null
    var title: String? = null
    var symbol: String? = null
    var price: BigDecimal? = null
    var change24h: Float? = null

    @JsonProperty("market_cap") var marketCap: Long? = null
    var volume: Long? = null
    var supply: Long? = null
    @JsonProperty("total_supply") var totalSupply: Long? = null
    var rank: Int? = null

    var logo: String? = null
    var website: String? = null
    var browser1: String? = null
    var browser2: String? = null
    var browser3: String? = null
    var src: String? = null

}


