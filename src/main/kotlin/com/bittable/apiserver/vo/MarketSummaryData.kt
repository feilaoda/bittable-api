package com.bittable.apiserver.vo

import io.swagger.annotations.ApiModelProperty
import java.math.BigDecimal

class MarketSummaryData {
    var usdcny: Float? = null
    var totalVolume: Long? = null
    var totalMarketCap: Long? = null
    var tops: List<MarketCoinSummary> ? = null
    var lastUpdateDate: Long? = null
}


class MarketCoinSummary {
    @ApiModelProperty("名称")
    var name: String? = null

    @ApiModelProperty("标题")
    var title: String? = null

    var symbol: String? = null

    @ApiModelProperty("价格(美元)")
    var price: BigDecimal? = null

}