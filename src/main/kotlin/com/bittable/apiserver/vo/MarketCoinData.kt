package com.bittable.apiserver.vo

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import java.math.BigDecimal

@ApiModel
class MarketCoinData {

    @ApiModelProperty("名称")
    var name: String? = null

    @ApiModelProperty("标题")
    var title: String? = null

    var symbol: String? = null

    var rank: Int? = null

    var logo: String? = null

    @ApiModelProperty("市值")
    var marketCap: Long? = null
    @ApiModelProperty("价格(美元)")
    var price: BigDecimal? = null
    @ApiModelProperty("交易量")
    var volume: Long? = null
    @ApiModelProperty("1h价格变化")
    var change1h: Float? = null
    @ApiModelProperty("6h价格变化")
    var change6h: Float? = null
    @ApiModelProperty("24h价格变化")
    var change24h: Float? = null
    @ApiModelProperty("7天价格变化")
    var change7d: Float? = null
    @ApiModelProperty("流通量")
    var supply: Long? = null

    @ApiModelProperty("7天价格趋势简图")
    var graphUrl: String? = null

}