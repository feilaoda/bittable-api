package com.bittable.apiserver.form

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import javax.validation.constraints.Max
import javax.validation.constraints.Min
import javax.validation.constraints.NotNull


@ApiModel
class MarketCoinForm (
) {
    @ApiModelProperty("分页")
    @Min(1)
    @Max(1000)
    var p : Int = 1
    @ApiModelProperty("分页大小")
    @Max(100)
    @Min(10)
    var size: Int = 100

    @ApiModelProperty("排序")
    var sort: String? = null
}


@ApiModel
class MarketTradeForm(
        @ApiModelProperty("开始时间戳，精确到毫秒")
        var start: Long,

        @ApiModelProperty("结束时间戳，精确到毫秒")
        var end: Long)
{

}