package com.bittable.apiserver.model

import io.ebean.Finder
import io.ebean.Model
import java.math.BigDecimal
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

open class BaseMarketTrade : Model(){



}

@Entity
@Table(name = "market_trade_min")
open class MarketTrade : Model(){
    @Id
    private var id: Long? = null

    @Column(name = "symbol_id")
    var symbolId: Int? = null

    @Column(name = "coin_id")
    var coinId: Int? = null

    @Column(name = "site_id")
    var siteId: Int? = null

    @Column(name = "data_type")
    var dataType: Int? = null

    @Column(name = "market_cap")
    var marketCapUsd: Long? = null

    @Column(name = "tid")
    var tid: Long? = null

    @Column(name = "price_date")
    var priceDate: Date? = null

    @Column(name = "price")
    var price: BigDecimal? = null

    @Column(name = "price_usd")
    var priceUsd: BigDecimal? = null

    @Column(name = "price_btc")
    var priceBtc: BigDecimal? = null

    @Column(name = "price_open")
    var priceOpen: BigDecimal? = null
    @Column(name = "price_close")
    var priceClose: BigDecimal? = null
    @Column(name = "price_high")
    var priceHigh: BigDecimal? = null
    @Column(name = "price_low")
    var priceLow: BigDecimal? = null

    @Column()
    var volume: Long? = null

    companion object {
        val finder = Finder<Long, MarketTrade>(MarketTrade::class.java)
    }
}

//@Entity
//@Table(name = "market_trade_5min")
//open class MarketTrade5Min : BaseMarketTrade(){
//    companion object {
//        val finder = Finder<Long, MarketTrade5Min>(MarketTrade5Min::class.java)
//    }
//}
//
//@Entity
//@Table(name = "market_trade_7day")
//open class MarketTrade7Day : BaseMarketTrade(){
//    companion object {
//        val finder = Finder<Long, MarketTrade7Day>(MarketTrade7Day::class.java)
//    }
//}
//
//@Entity
//@Table(name = "market_trade_1mon")
//open class MarketTrade1Month : BaseMarketTrade(){
//    companion object {
//        val finder = Finder<Long, MarketTrade1Month>(MarketTrade1Month::class.java)
//    }
//}
//
//
//@Entity
//@Table(name = "market_trade_3mon")
//open class MarketTrade3Month : BaseMarketTrade(){
//    companion object {
//        val finder = Finder<Long, MarketTrade3Month>(MarketTrade3Month::class.java)
//    }
//}
//
//@Entity
//@Table(name = "market_trade_1year")
//open class MarketTrade1Year : BaseMarketTrade(){
//    companion object {
//        val finder = Finder<Long, MarketTrade1Year>(MarketTrade1Year::class.java)
//    }
//}
//
//@Entity
//@Table(name = "market_trade_all")
//open class MarketTradeAll : BaseMarketTrade(){
//    companion object {
//        val finder = Finder<Long, MarketTradeAll>(MarketTradeAll::class.java)
//    }
//}
