package com.bittable.apiserver.model

import io.ebean.Finder
import io.ebean.Model
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import java.math.BigDecimal
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "market_coin")
open class MarketCoin : Model(){

    @Id
    var id: Long? = null

    @Column(name = "name")
    var name: String? = null

    @Column(name = "title")
    var title: String? = null

    @ApiModelProperty("")
    @Column(name = "symbol")
    var symbol: String? = null

    @Column
    var rank: Int? = null

    @Column
    var logo: String? = null

    @ApiModelProperty("市值")
    @Column(name = "market_cap")
    var marketCap: Long? = null

    @ApiModelProperty("价格")
    @Column(name = "price")
    var price: BigDecimal? = null

    @Column(name = "supply")
    var supply: Long? = null


    @ApiModelProperty("交易量(24h)")
    @Column(name="volume")
    var volume: Long? = null

    @Column(name = "change1h")
    var change1h: BigDecimal? = null

    @Column(name = "change6h")
    var change6h: BigDecimal? = null

    @Column(name = "change24h")
    var change24h: BigDecimal? = null

    @Column(name = "change7d")
    var change7d: BigDecimal? = null

    @Column(name="src")
    var src: String? = null

    @Column(name="website")
    var website: String? = null

    @Column(name="browser1")
    var browser1: String? = null

    @Column(name="browser2")
    var browser2: String? = null

    @Column(name="browser3")
    var browser3: String? = null

    companion object {
        val finder = Finder<Long, MarketCoin>(MarketCoin::class.java)
    }

}